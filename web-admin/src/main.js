// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'
import './common.css'

Vue.use(ElementUI)
// Vue.config.productionTip = false

import Api from './utils/Api'
import Http from './utils/Http'
import moment from 'moment'
window.Lucifer={
	v:'0.1.0',
	api:Api,
	http:Http,
	ImgDomin:'http://admin.luciferchina.com'
}
window.moment=moment;
Vue.prototype.$moment = moment;

import store from './store/store'

/* eslint-disable no-new */
Lucifer.app = new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})



