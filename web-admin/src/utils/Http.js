import Vue from 'vue'
import axios from 'axios'

let baseURL = '';
if(location.href.indexOf('localhost')!=-1){
	baseURL = 'http://localhost:8080';
}else{
	baseURL = 'http://api.luciferchina.com';
}
let Http = {
	baseURL: baseURL,
	header: {
		Authorization: localStorage.getItem('luc-admin-token'),
		'Content-Type': 'application/json; charset=utf-8'
	},
	login(api, data) {
		let method = api[0];
		return axios({
			method: method,
			url: api[1],
			data: data,
			baseURL: baseURL,
			headers: this.header
		})
	},
	getUser() {
		return axios({
			method: 'get',
			url: '/biz/getStaff',
			baseURL: baseURL,
			headers: this.header
		})
	},
	test() {
		console.log(123)
		Lucifer.app.$notify.error({
			title: '消息',
			message: '这是一条消息的提示消息'
		});
	},
	/**
	 * 程序中请求数据用此方法
	 * @param  {array}   api  Apis中的数据    
	 * @param  {object}   data     传参
	 * @param  {Function} callback 回调函数
	 * @return {[type]}            
	 */
	request(api, data, callback) {
		/**
		 * 判断token是否存在
		 * 如果存在 继续请求
		 *
		 * 如果不存在 说明用户未登录 或是已经退出 此时应提示信息
		 */
		if (api instanceof Array) {
			var reqBody = {}, reqBodyKeys = Object.keys(data)
			reqBodyKeys.forEach(function (item) {
				if (data[item] !== '' && data[item] !== null) {
					reqBody[item] = data[item];
				}
			})
			let method = api[0];
			let req = {
				method: method,
				url: api[1],
				baseURL: baseURL,
				headers: this.header
			};
			if (method == 'get') {
				req.params = reqBody
			} else {
				req.data = reqBody
			}
			return axios(req).then(function (res) {
				if (res.data.status == 200) {
					if (callback) {
						callback(res.data.body)
					}
				} else {
					Lucifer.app.$notify.error({
						title: res.data.status,
						message: res.data.err.message
					});
				}
			}).catch(function (err) {
				Lucifer.app.$notify.error({
					title: "error",
					message: "服务器出错了"
				});
			}.bind(this));
		}

	}

}

export default Http
