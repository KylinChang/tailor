import Vue from 'vue'
import Vuex from 'vuex'


Vue.use(Vuex)

const store = new Vuex.Store({
	state:{
		user:null,
		breadcrumbs:[]
	},
	mutations:{
		login(state,userdata){
			state.user = userdata
		},
		updateBreadcrumb(state,arr){
			state.breadcrumbs = arr
		}
	}
})

export default store
