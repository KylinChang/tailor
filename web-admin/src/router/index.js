import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/views/Home'
import Login from '@/views/Login'

Vue.use(Router)

// lazyload

// index
const Index = resolve => require(['../views/index/index'], resolve)
const Index2 = resolve => require(['../views/index/detail'], resolve)

// order
const Orders = resolve => require(['../views/order/orders'], resolve)
const Order = resolve => require(['../views/order/order'], resolve)
const ReOrders = resolve => require(['../views/order/reOrders'], resolve)
const ReOrder = resolve => require(['../views/order/reOrder'], resolve)
const OrderTotals = resolve => require(['../views/order/orderTotals'], resolve)
const OrderSearch = resolve => require(['../views/order/search'], resolve)

// user
const Customers = resolve => require(['../views/user/customers'], resolve)
const Customer = resolve => require(['../views/user/customer'], resolve)
const Bokers = resolve => require(['../views/user/bokers'], resolve)
const Boker = resolve => require(['../views/user/boker'], resolve)
const BokerReviews = resolve => require(['../views/user/bokerReviews'], resolve)
const BokerReview = resolve => require(['../views/user/bokerReview'], resolve)

// news
const News = resolve => require(['../views/news/list'], resolve)
const NewsDetail = resolve => require(['../views/news/detail'], resolve)

// factory
const Factorys = resolve => require(['../views/factorys/list'], resolve)
const FactoryDetail = resolve => require(['../views/factorys/detail'], resolve)

// labels
const Labels = resolve => require(['../views/labels/list'], resolve)

// system
const Changepwd = resolve => require(['../views/system/changepwd'], resolve)
    // keeplive 是否缓存当前页面  一般list设为true 详情页为false
    // permission  权限设置
export default new Router({
  routes: [{
    path: '/',
    component: Home,
    children: [{
      path: '/',
      name: 'Index',
      component: Index,
      meta: {
        keepAlive: true,
        permission: 10
      }
    }, {
      path: '/index2',
      name: 'Index2',
      component: Index2,
      meta: {
        keepAlive: true,
        permission: 10
      }
    }, {
      path: '/order/orders',
      name: 'Orders',
      component: Orders,
      meta: {
        keepAlive: true,
        permission: 10
      }
    }, {
      path: '/order/order/:id',
      name: 'Order',
      component: Order,
      meta: {
        keepAlive: true,
        permission: 10
      }
    }, {
      path: '/order/order',
      name: 'addOrder',
      component: Order,
      meta: {
        keepAlive: false,
        permission: 10
      }
    }, {
      path: '/order/reOrders',
      name: 'ReOrders',
      component: ReOrders,
      meta: {
        keepAlive: true,
        permission: 10
      }
    }, {
      path: '/order/reOrder/:id',
      name: 'ReOrder',
      component: ReOrder,
      meta: {
        keepAlive: false,
        permission: 10
      }
    }, {
      path: '/order/orderTotals',
      name: 'OrderTotals',
      component: OrderTotals,
      meta: {
        keepAlive: true,
        permission: 10
      }
    }, {
      path: '/order/search',
      name: 'OrderSearch',
      component: OrderSearch,
      meta: {
        keepAlive: true,
        permission: 10
      }
    }, {
      path: '/user/customers',
      name: 'Customers',
      component: Customers,
      meta: {
        keepAlive: true,
        permission: 10
      }
    }, {
      path: '/user/customer/:id',
      name: 'Customer',
      component: Customer,
      meta: {
        keepAlive: false,
        permission: 10
      }
    }, {
      path: '/user/bokers',
      name: 'Bokers',
      component: Bokers,
      meta: {
        keepAlive: true,
        permission: 10
      }
    }, {
      path: '/user/boker/id',
      name: 'Boker',
      component: Boker,
      meta: {
        keepAlive: false,
        permission: 10
      }
    }, {
      path: '/user/boker',
      name: 'addBoker',
      component: Boker,
      meta: {
        keepAlive: false,
        permission: 10
      }
    }, {
      path: '/user/bokerReviews',
      name: 'BokerReviews',
      component: BokerReviews,
      meta: {
        keepAlive: true,
        permission: 10
      }
    }, {
      path: '/user/BokerReview/:id',
      name: 'BokerReview',
      component: BokerReview,
      meta: {
        keepAlive: false,
        permission: 10
      }
    }, {
      path: '/news/list',
      name: 'News',
      component: News,
      meta: {
        keepAlive: true,
        permission: 10
      }
    }, {
      path: '/news/detail/:id',
      name: 'NewsDetail',
      component: NewsDetail,
      meta: {
        keepAlive: false,
        permission: 10
      }
    }, {
      path: '/news/add/',
      name: 'addNewsDetail',
      component: NewsDetail,
      meta: {
        keepAlive: false,
        permission: 10
      }
    }, {
      path: '/factory/list',
      name: 'Factorys',
      component: Factorys,
      meta: {
        keepAlive: true,
        permission: 10
      }
    }, {
      path: '/factory/detail/:id',
      name: 'FactoryDetail',
      component: FactoryDetail,
      meta: {
        keepAlive: false,
        permission: 10
      }
    }, {
      path: '/factory/add/',
      name: 'addFactoryDetail',
      component: FactoryDetail,
      meta: {
        keepAlive: false,
        permission: 10
      }
    }, {
      path: '/labels/list',
      name: 'Labels',
      component: Labels,
      meta: {
        keepAlive: true,
        permission: 10
      }
    }, {
      path: '/system/changepwd',
      name: 'Changepwd',
      component: Changepwd,
      meta: {
        keepAlive: false,
        permission: 10
      }
    }]
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    permission: 10
  }
  ]
})
